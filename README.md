# Notepad App

> An electron-vue project

#### Build Setup

``` bash
# install dependencies
yarn install

# serve with hot reload at localhost:9080
npm run dev

# build electron application for production
npm run build

# run unit & end-to-end tests
npm run test

# make zip archive
npm run zip

```
