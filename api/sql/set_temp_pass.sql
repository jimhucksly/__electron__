CREATE OR REPLACE FUNCTION SET_TEMP_PASS(
	_USER_ID int, 
	_PASS CHAR(64)
) RETURNS setof temp_password
AS $$

BEGIN
	IF(
		EXISTS(SELECT * FROM TEMP_PASSWORD WHERE USER_ID = $1)
	) 
	THEN
		UPDATE TEMP_PASSWORD SET TEMP_PSWD_MD5 = MD5($2);
	ELSE
		INSERT INTO TEMP_PASSWORD VALUES($1, MD5($2));
	END IF;
	
	RETURN QUERY (SELECT * FROM TEMP_PASSWORD WHERE USER_ID = $1);
	
END 
 	$$ LANGUAGE PLPGSQL;
