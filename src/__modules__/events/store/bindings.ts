const bindings = {
  EventsQuery: Symbol.for('EventsQuery'),
  UpdateEventCommand: Symbol.for('UpdateEventCommand'),
  DeleteEventCommand: Symbol.for('DeleteEventCommand')
}

export {
  bindings
}
