import { ITodoState } from '../models'

export const stateKeys: string[] = [
  'todo'
]

const state: ITodoState = {
  todo: null
}

export default state
