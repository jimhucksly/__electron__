const bindings = {
  LibraryQuery: Symbol.for('LibraryQuery'),
  LibraryFilesQuery: Symbol.for('LibraryFilesQuery'),
  LibraryFileQuery: Symbol.for('LibraryFileQuery'),
  AddLibraryFileCommand: Symbol.for('AddLibraryFileCommand'),
  UpdateLibraryCommand: Symbol.for('UpdateLibraryCommand'),
  DeleteLibraryFileCommand: Symbol.for('DeleteLibraryFileCommand')
}

export {
  bindings
}
