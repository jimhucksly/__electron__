export interface ILink {
  id?: string
  url: string
  name: string
}

export interface ILinksState {
  links: ILink
}
