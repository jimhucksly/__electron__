import { ILinksState } from '../models'

export const stateKeys: string[] = [
  'links'
]

const state: ILinksState = {
  links: null
}

export default state
