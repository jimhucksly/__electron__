const bindings = {
  LinksQuery: Symbol.for('LinksQuery'),
  UpdateLinksCommand: Symbol.for('UpdateLinksCommand'),
  DeleteLinkCommand: Symbol.for('DeleteLinkCommand')
}

export {
  bindings
}
