import { IFilesState } from '../models'

export const stateKeys: string[] = [
  'files'
]

const state: IFilesState = {
  files: null
}

export default state
