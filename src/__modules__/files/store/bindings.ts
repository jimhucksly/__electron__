const bindings = {
  FilesQuery: Symbol.for('FilesQuery'),
  DeleteFileCommand: Symbol.for('DeleteFileCommand'),
  UploadFileCommand: Symbol.for('UploadFileCommand')
}

export {
  bindings
}
